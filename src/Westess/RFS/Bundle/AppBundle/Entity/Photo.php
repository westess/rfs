<?php

namespace Westess\RFS\Bundle\AppBundle\Entity;

use Westess\RFS\Bundle\AppBundle\Resources\S3;
use Aws\ConfigService\Exception\ConfigServiceException;
use Aws\S3\S3Client;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Photo
 * @ExclusionPolicy("all")
 */
class Photo
{
    /**
     * @var integer
     * @Expose
     * @Type("integer")
     */
    private $id;

    /**
     * @var boolean
     */
    private $visible;

    /**
     * @var \Westess\RFS\Bundle\AppBundle\Entity\Accommodation
     */
    private $accommodation;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Photo
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set accommodation
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Accommodation $accommodation
     * @return Photo
     */
    public function setAccommodation(\Westess\RFS\Bundle\AppBundle\Entity\Accommodation $accommodation = null)
    {
        $this->accommodation = $accommodation;

        return $this;
    }

    /**
     * Get accommodation
     *
     * @return \Westess\RFS\Bundle\AppBundle\Entity\Accommodation
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    private $temp;
    private static $uploadRootDir = 'uploads/images/accommodation';

    /**
     * @Assert\File(maxSize="10M")
     */
    private $file;

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAppPath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAppPath();
        }
    }

    /**
     * @ORM\PrePersist()
     *
     */
    public function preUpload()
    {
        if (null == $this->getFile()) {
            throw new \RuntimeException("Trying to upload non-existent file");
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->temp);
            // clear the temp image path
            $this->temp = null;
        }

        //UploadFile move() throws an exception if the file cannot be moved so that the entity is not persisted to the database
        $this->getFile()->move(self::$uploadRootDir . '/' . $this->id, $this->id . '.jpg');

        $this->setFile(null);
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->temp = $this->getAppPath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $this->removeFromBucket();
        if (isset($this->temp)) {
            unlink($this->temp);
        }
    }

    public function getFileRootPath()
    {
        return null === $this->id
            ? null
            : self::$uploadRootDir . '/' . $this->id;
    }

    public function getAppPath()
    {
        return null === $this->id
            ? null
            : self::$uploadRootDir . '/' . $this->id . '/' . $this->id . '.jpg';
    }

    public function getWebPath()
    {
        return null === $this->id
            ? null
            : 'https://s3-eu-west-1.amazonaws.com/rfs-assets' . '/' . self::getAppPath();
    }

    public function saveToBucket()
    {
        putenv("AWS_ACCESS_KEY_ID=AKIAJENPX4O5EIXQXBHQ");
        putenv("AWS_SECRET_ACCESS_KEY=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");
        putenv("S3_BUCKET_NAME=rfs-assets");
        $s3 = new S3(getenv('AWS_ACCESS_KEY_ID'), getenv('AWS_SECRET_ACCESS_KEY'));

        if (!$s3->putObjectFile($this->getAppPath(), getenv("S3_BUCKET_NAME"), $this->getAppPath(), S3::ACL_PUBLIC_READ)) {
            throw new ConfigServiceException('No "S3_BUCKET" config var in found in env!');
        }
    }

    public function removeFromBucket()
    {
        $s3 = new S3(getenv('AWS_ACCESS_KEY_ID'), getenv('AWS_SECRET_ACCESS_KEY'));
        if (!$s3->deleteObject(getenv("S3_BUCKET_NAME"), $this->temp)) {
            throw new ConfigServiceException('No "S3_BUCKET" config var in found in env!');
        }
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
