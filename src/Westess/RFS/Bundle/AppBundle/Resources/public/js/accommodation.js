// Facilitates an autocomplete field and map for user to enter a valid accommodation address
$(function () {
    var submitMap;
    var marker = new google.maps.Marker();
    var autocomplete;
    var autocompleteField = $('#rfs_accommodation_fullAddress')[0];
    var countryRestrict = {'country': 'gb'};
    var MARKER_PATH = 'https://maps.gstatic.com/intl/en_us/mapfiles/marker_green';
    var hostnameRegexp = new RegExp('^https?://.+?/');

    function initialize() {
        var myOptions = {
            zoom: 7,
            center: new google.maps.LatLng(52.621132, -1.124640),
            mapTypeControl: false,
            panControl: false,
            zoomControl: true,
            streetViewControl: true
        };
        submitMap = new google.maps.Map($('#map')[0], myOptions);

        // Create the autocomplete object and associate it with the UI input control.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */(autocompleteField),
            {types: [], componentRestrictions: countryRestrict});//Restrict the search to UAE, and to place type "address".
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            onPlaceChanged();
            fillInAddress();
        });
    }

    // When the user selects an address, get the place details for the address,
    // zoom the map in on the address and drop a pin on the place.
    function onPlaceChanged() {
        var place = autocomplete.getPlace();
        if (place.geometry) {
            submitMap.panTo(place.geometry.location);
            submitMap.setZoom(15);
            marker.setMap(null);
            var markerIcon = MARKER_PATH + 'R' + '.png';
            marker = new google.maps.Marker({
                map: submitMap,
                position: place.geometry.location,
                animation: google.maps.Animation.DROP,
                icon: markerIcon
            });
        } else {
            autocompleteField.placeholder = 'Enter address';
        }
    }

    // Populate hidden address form
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        $('#rfs_accommodation_postcode')[0].value = place.address_components[6].long_name;
        $('#rfs_accommodation_longitude')[0].value = place.geometry.location.F;
        $('#rfs_accommodation_latitude')[0].value = place.geometry.location.A;
    }

    $(document).ready(function () {
        initialize();
    });
});


// Facilitates add features with modal :(
$(function () {
    var $collectionHolder;

    // setup an "add a feature" link
    var $addFeatureLink = $('#add-feature-link');

    function addFeatureForm($collectionHolder) {
        // Get the data-prototype
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li
        var $newFormLi = $('<li></li>').append(newForm);

        // add the new form li to the features ul
        $collectionHolder.append($newFormLi).append('<hr>');
    }

    $(document).ready(function () {
        // Get the ul that holds the collection of features
        $collectionHolder = $('ul.features');

        // count the current form inputs we have (e.g. 2), use that as the new
        // index when inserting a new item (e.g. 2)
        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addFeatureLink.on('click', function (e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // add a new feature form
            addFeatureForm($collectionHolder);
        });
    });
});

// Facilitates add photos with modal :)
$(function () {
    var $collectionHolder;

    // setup an "add a photo" link
    var $addPhotoLink = $('#add-photo-link');

    var index = 0;

    function addPhotoForm($collectionHolder) {
        // Get the data-prototype
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li
        var $newFormLi = $('<li></li>').append(newForm);

        // add the new form li to the photos ul
        $collectionHolder.append($newFormLi).append('<hr>');
    }

    $(document).ready(function () {
        // Get the ul that holds the collection of photos
        $collectionHolder = $('ul.photos');

        // count the current form inputs we have (e.g. 2), use that as the new
        // index when inserting a new item (e.g. 2)
        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addPhotoLink.on('click', function (e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // add a new photo form (see next code block)
            addPhotoForm($collectionHolder);
        });
    });
});