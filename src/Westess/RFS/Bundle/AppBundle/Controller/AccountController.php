<?php
//git check
namespace Westess\RFS\Bundle\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Westess\RFS\Bundle\AppBundle\Entity\Accommodation;
use Application\Sonata\UserBundle\Entity\User;


class AccountController extends Controller
{
  /**
   * @Route("/account", name="edit_account")
   * @Template("RFSAppBundle:Accounts:edit-account.html.twig")
   */
   function editAccountAction(Request $request)
   {
     if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
         $session = $request->getSession();
         $session->getFlashBag()->add('info', 'You must be logged in to use this feature');
         throw $this->createAccessDeniedException();
     }
     return;
   }

   /**
    * @Route("/account/edit", name="edit_account_execute")
    */
    function editAccountExecuteAction(Request $request)
    {
      if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER'))
      {
        $session = $request->getSession();
        $session->getFlashBag()->add('info', 'You must be logged in to use this feature');
        throw $this->createAccessDeniedException();
      }
      $em = $this->getDoctrine()->getManager();
      $user = $this->getUser();

      $user->setFirstName($request->get("fname"));
      $user->setLastName($request->get("lname"));

      $em->persist($user);
      $em->flush();

      return $this->redirect("/account");
    }

    /**
     * @Route("/show/profile/{username}")
     * @Template("RFSAppBundle:Accounts:show-profile.html.twig");
     */
     function showProfileAction(Request $request, $username)
     {
       if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER'))
       {
         $session = $request->getSession();
         $session->getFlashBag()->add('info', 'You must be logged in to use this feature');
         throw $this->createAccessDeniedException();
       }
       $em = $this->getDoctrine()->getManager();

       $userRecord = $em->getRepository("ApplicationSonataUserBundle:User")->findOneByUsername($username);

       return array("userProfile" => $userRecord);
     }
}
