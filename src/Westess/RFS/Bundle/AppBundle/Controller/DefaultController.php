<?php

namespace Westess\RFS\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Westess\RFS\Bundle\AppBundle\Resources\Referer;

class DefaultController extends Controller
{
    use Referer;

    public function createSearchForm()
    {
        $form = $this->createFormBuilder()
            ->add('uni', 'choice', array('label' => false, 'choices' => array('le' => 'University of Leicester', 'dmu' => 'De Montfort University'), 'multiple' => false, 'attr' => array('class' => 'form-control', 'validation_groups' => false)))
            ->add('search', 'submit', array('label' => 'Search', 'attr' => array('class' => 'btn btn-primary')))
            ->getForm();
        return $form;
    }

    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $params = $this->getRefererParams();
        if ($params['_route'] == 'login' && $this->get('security.authorization_checker')->isGranted('ROLE_LANDLORD')) {
            return $this->redirect('/properties/user');
        }

        $searchForm = $this->createSearchForm();
        $searchForm->handleRequest($request);
        if ($searchForm->isValid()) {
            $uni = $searchForm->get('uni')->getData();
            return $this->redirect($this->generateUrl("search", array("uni" => $uni)));
        }

        return $this->render('RFSAppBundle:Default:index.html.twig', array('form' => $searchForm->createView()));
    }
}
