var primaryColor = '#7AC6EF';
var mapGLOBAL;
var defaultZoom = 14;
$(function () {
    $(window).scroll(function () {
        /***********************************************************
         * HEADER BOX SHADOW
         ***********************************************************/
        if ($(this).scrollTop() == 0) {
            $('#header').css({
                'box-shadow': 'none',
                '-moz-box-shadow': 'none',
                '-webkit-box-shadow': 'none'
            });
        }
        else {
            $('#header').css({
                'box-shadow': '0px 5px 5px #777',
                '-moz-box-shadow': '0px 5px 5px #777',
                '-webkit-box-shadow': '0px 5px 5px #777'
            });
        }
    });
});
