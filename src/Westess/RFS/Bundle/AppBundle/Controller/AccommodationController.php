<?php

namespace Westess\RFS\Bundle\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SendGrid\Email;
use Westess\RFS\Bundle\AppBundle\Entity\Accommodation;
use Westess\RFS\Bundle\AppBundle\Form\AccommodationType;
use Westess\RFS\Bundle\AppBundle\Entity\UserAccommodationInterest;

/**
 * Accommodation controller.
 *
 * @Route("/accommodation")
 */
class AccommodationController extends Controller
{


  /**
   * @Route("/favourite/{id}")
   */
   function accommodationFavouriteAction(Request $request, $id)
   {
     if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
         $session = $request->getSession();
         $session->getFlashBag()->add('info', 'You must be logged in to use this feature');
         throw $this->createAccessDeniedException();
     }
     $em = $this->getDoctrine()->getManager();

     $user= $this->getUser();
     $accommodation = $em->getRepository("RFSAppBundle:Accommodation")->findOneById($id);

     //check if a favourite already exists. if not create one, otherwise delete it.
     $uaInterestPossible = $em->getRepository("RFSAppBundle:UserAccommodationInterest")->findOneBy(array("user" => $user, "accommodation" => $accommodation));
     if($uaInterestPossible == NULL)
     {
       $uaInterest = new UserAccommodationInterest();

       $uaInterest->setUser($user);
       $uaInterest->setAccommodation($accommodation);

       $em->persist($uaInterest);
       $em->flush();
       return new Response("t");
     }
     else
     {
        $em->remove($uaInterestPossible);
        $em->flush();
        return new Response("f");
     }
   }

   /**
    * @Route("/favourite/{id}/readonly")
    */
    function accommodationFavouriteReadOnlyAction(Request $request, $id)
    {
      if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
          $session = $request->getSession();
          $session->getFlashBag()->add('info', 'You must be logged in to use this feature');
          throw $this->createAccessDeniedException();
      }
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();
      $accommodation = $em->getRepository("RFSAppBundle:Accommodation")->findOneById($id);

      //check if a favourite exists return 't' if it does, 'f' is it does not.
      $uaInterest = $em->getRepository("RFSAppBundle:UserAccommodationInterest")->findOneBy(array("user" => $user, "accommodation" => $accommodation));
      if($uaInterest == NULL)
      {
        return new Response("f");
      }
      else
      {
          return new Response("t");
      }
    }

    /**
     * Lists all Accommodation entities.
     *
     * @Route("/", name="accommodation")
     * @Method("GET")
     * @Template("RFSAppBundle:Accommodation:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RFSAppBundle:Accommodation')->findAll();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array('entities' => $entities);
    }

    /**
     * @Route("/list/{name}", name="my_accommodations")
     * @Template("RFSAppBundle:Accommodation:index-user.html.twig")
     */
    public function myPropertyAction()
    {
        $this->denyAccessUnlessGranted('ROLE_LANDLORD', null, 'Unauthorized access');

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('RFSAppBundle:Accommodation')->createQueryBuilder('a');
        $accommodations = $qb->where($qb->expr()->eq('a.user', ':user'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
        return array('entities' => $accommodations);
    }


    /**
     * Creates a new Accommodation entity.
     *
     * @Route("/", name="accommodation_create")
     * @Method("POST")
     * @Template("RFSAppBundle:Accommodation:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LANDLORD', null, 'Unauthorized access');

        $entity = new Accommodation();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();
            $entity->setUser($user);

            $em->persist($entity);
            $em->flush();

            $photo = $entity->getMainPhoto();
            $photo->setAccommodation($entity);

            foreach ($entity->getPhotos() as $photo) {
                $photo->setAccommodation($entity);
            }

            foreach ($entity->getFeatures() as $feature) {
                $feature->setAccommodation($entity);
            }

            $entity->setViewCount(0);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('accommodation_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Accommodation entity.
     *
     * @param Accommodation $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Accommodation $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new AccommodationType($em), $entity, array(
            'action' => $this->generateUrl('accommodation_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Accommodation entity.
     *
     * @Route("/new", name="accommodation_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->denyAccessUnlessGranted('ROLE_LANDLORD', null, 'Unauthorized access');

        $entity = new Accommodation();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Accommodation entity.
     *
     * @Route("/{id}", name="accommodation_show")
     * @Method("GET")
     * @Template("RFSAppBundle:Accommodation:show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RFSAppBundle:Accommodation')->find($id);

        $viewCount = $entity->getViewCount();
        $entity->setViewCount($viewCount + 1);

        $em->persist($entity);
        $em->flush();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Accommodation entity.');
        }

        //generate list of users who favourited this accommodation
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')){
          $query = $em->createQueryBuilder();
          $userFavourites = $query
              ->select(array("u"))
              ->from("RFSAppBundle:UserAccommodationInterest", "u")
              ->where($query->expr()->neq("u.user", ":thisUser"))
              ->setParameter("thisUser", $this->getUser())
              ->getQuery()->getResult();
          return array('accommodation' => $entity, 'userFavourites' => $userFavourites);
         }

        return array('accommodation' => $entity);
    }

    /**
     * Displays a form to edit an existing Accommodation entity.
     *
     * @Route("/{id}/edit", name="accommodation_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->denyAccessUnlessGranted('ROLE_LANDLORD', null, 'Unauthorized access');

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RFSAppBundle:Accommodation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Accommodation entity.');
        }

        if ($this->getUser()->getId() != $entity->getUser()->getId()) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unauthorized access');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Accommodation entity.
     *
     * @param Accommodation $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Accommodation $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new AccommodationType($em), $entity, array(
            'action' => $this->generateUrl('accommodation_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Accommodation entity.
     *
     * @Route("/{id}", name="accommodation_update")
     * @Method("PUT")
     * @Template("RFSAppBundle:Accommodation:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_LANDLORD', null, 'Unauthorized access');

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RFSAppBundle:Accommodation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Accommodation entity.');
        }

        if ($this->getUser()->getId() != $entity->getUser()->getId()) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unauthorized access');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('accommodation_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Accommodation entity.
     *
     * @Route("/{id}", name="accommodation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_LANDLORD', null, 'Unauthorized access');

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RFSAppBundle:Accommodation')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Accommodation entity.');
            }

            if ($this->getUser()->getId() != $entity->getUser()->getId()) {
                $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unauthorized access');
            }

            //delete property main photos
            $entity->setMainPhoto(null);

            //delete property photos
            foreach ($entity->getPhotos() as $photo) {
                $photo->setProperty(null);
                $entity->removePhoto($photo);
                $em->remove($photo);
            }

            //delete property features
            foreach ($entity->getFeatures() as $feature) {
                $feature->setAccommodation(null);
                $entity->removeFeature($feature);
                $em->remove($feature);
            }

            $em->flush();

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('accommodation'));
    }

    /**
     * Creates a form to delete a Accommodation entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('accommodation_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * @Route("/hide/{id}")
     * @Method("PUT")
     */
    public function hideAction($id)
    {
        $this->denyAccessUnlessGranted('ROLE_LANDLORD', null, 'Unauthorized access');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RFSAppBundle:Accommodation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Property entity.');
        }

        if ($this->getUser()->getId() != $entity->getUser()->getId()) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unauthorized access');
        }

        $entity->setIsVisible(0);
        $em->persist($entity);
        $em->flush();

        return new Response('hidden', Response::HTTP_OK,
            array('content-type' => 'text/plain'));
    }

    /**
     * @Route("/show/{id}")
     * @Method("PUT")
     */
    public function show_Action($id)
    {
        $this->denyAccessUnlessGranted('ROLE_LANDLORD', null, 'Unauthorized access');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RFSAppBundle:Accommodation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Property entity.');
        }

        if ($this->getUser()->getId() != $entity->getUser()->getId()) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unauthorized access');
        }

        $entity->setIsVisible(1);
        $em->persist($entity);
        $em->flush();

        return new Response('shown', Response::HTTP_OK,
            array('content-type' => 'text/plain'));
    }

    /**
     * @Route("/enquiry/{id}", name="accommodation_enquiry")
     *
     */
    public function enquiry(Request $request, $id)
    {
        //mailer protocol is https, set this option to avoid ssl verification errors
        \Unirest::verifyPeer(false);

        $mailer = $this->get('send_grid_mailer');
        $email = new Email();
        $email->addTo('teerexinc@gmail.com')
            ->addTo('help@westess.co.uk')
            ->setFrom('enquiry@westess.com')
            ->setSubject("Accommodation Enquiry")
            ->setHtml("You have received a new message from your website enquiry form.<br><br>" .
                "Here are the details: <br><br>" .
                "Email: " . $request->get('form_email') . "<br><br>" .
                "Message:<br>" . $request->get('form_message') . "<br><br>" .
                "Property: http://uaedxb.com/property/$id");
        $mailer->send($email);
        $request->getSession()->getFlashBag()->add('info', 'Enquiry sent, we will contact you soon.');
        return $this->redirect($this->generateUrl('accommodation_show', array('id' => $id)));
    }
}
