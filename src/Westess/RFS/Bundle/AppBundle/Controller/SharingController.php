<?php
//git check
namespace Westess\RFS\Bundle\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SendGrid\Email;
use Westess\RFS\Bundle\AppBundle\Entity\Accommodation;
use Westess\RFS\Bundle\AppBundle\Entity\LinkedUser;
use Westess\RFS\Bundle\AppBundle\Entity\Share;
use Westess\RFS\Bundle\AppBundle\Entity\ShareUser;
use Westess\RFS\Bundle\AppBundle\Form\AccommodationType;
use Application\Sonata\UserBundle\Entity\User;

/**
 * Accommodation controller.
 *
 * @Route("/share")
 */
class SharingController extends Controller
{

  /**
   * @Route("/view-accommodation/{shareUserId}", name="view_shared_accommodation")
   */
   public function viewSharedAccommodationAction($shareUserId)
   {
     $em = $this->getDoctrine()->getManager();

     $shareUser = $em->getRepository("RFSAppBundle:ShareUser")->findOneById($shareUserId);

     $shareUser->setHasSeen(true);
     $em->persist($shareUser);
     $em->flush();

     return $this->redirect("/accommodation/" . $shareUser->getShare()->getAccommodation()->getId());
   }

  /**
   * @Route("/notifications", name="sharing_notifications")
   * @Template("RFSAppBundle:Sharing:notification-centre.html.twig")
   */
   public function notificationCentreAction()
   {
     $em = $this->getDoctrine()->getManager();
     $userShares = $em->getRepository("RFSAppBundle:ShareUser")->findBy(array("user" =>$this->getUser(), "landlordContact" => 0));

     return array("shares" => $userShares);
   }

  /**
   * @Route("/email/{id}", name="share_via_email_popup")
   * @Template("RFSAppBundle:Sharing:email-popup.html.twig")
   * @Method("GET")
   */
  public function shareEmailAction(Request $request, $id)
  {
    $session = $request->getSession();

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $session->getFlashBag()->add('info', 'You must be logged in to use this feature');
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository('RFSAppBundle:Accommodation')->findOneById($id);

        return array("property" => $property);
  }

  /**
   *  @Route("/email/{id}", name="share_send_email")
   */
  public function sendShareEmailAction(Request $request, $id)
  {

    //return new Response("Email sent");

    if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
          throw $this->createAccessDeniedException();
      }

      $emailToFullString = $request->get("form_to_addresses");

      $emailSplit = str_replace(" ", "", $emailToFullString);
      //$emailSplit = str_replace(",", "<br/>", $emailSplit);

      $emailSplitArray = explode(",", $emailSplit);

      //return new Response($emailSplitArray[0]);

      $message = $request->get("form_message");

      $em = $this->getDoctrine()->getManager();
      $property = $em->getRepository("AppBundle:Properties")->findOneById($request->get("property_id"));

      //mailer protocol is https, set this option to avoid ssl verification errors
      \Unirest::verifyPeer(false);

      $mailer = $this->get('send_grid_mailer');
      $email = new Email();

      for($i = 0; $i < sizeof($emailSplitArray); $i++)
      {
          $email->addTo($emailSplitArray[$i]);
      }

      $email->setSubject('fName lName shared a property with you')
          ->setFrom('roomforstudents.co.uk')
          ->setHtml($this->renderView('RFSAppBundle:Sharing:share-email-template.html.twig', array("property" => $property, "message" => $message)), 'text/html');
      $mailer->send($email);

      return new Response("Email sent");
  }

  /**
   * @Route("/user/{id}", name="share_via_user_popup")
   * @Template("RFSAppBundle:Sharing:user-popup.html.twig")
   */
  public function shareUserAction(Request $request, $id)
  {
    $session = $request->getSession();

        $session->set("property_id", $id);

        if(!$this->get('security.authorization_checker')->isGranted("ROLE_USER"))
        {
            $session->getFlashBag()->add("info", "You must be logged in to use this feature");
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository("RFSAppBundle:Accommodation")->findOneById($id);

        //$user = new User();
        //$user = $this->getUser()->getId();

        //find users you are linked to
        $user = $this->getUser();

        $linkedUsersOne = $em->getRepository("RFSAppBundle:LinkedUser")->findByUserOne($user);
        $linkedUsersTwo = $em->getRepository("RFSAppBundle:LinkedUser")->findByUserTwo($user);


        //find users who have favourited this property so you can message them
        $query = $em->createQueryBuilder();
        /*$userFavourites = $query
            ->select(array("u"))
            ->from("RFSAppBundle:UserAccommodationInterest", "u")
            ->from("RFSAppBundle:LinkedUser", "l")
            ->where(
              $query->expr()->andX(
                $query->expr()->andX(
                  $query->expr()->neq("u.user", ":thisUser"),
                  $query->expr()->neq("u.user", "l.userOne"),
                ),
                $query->expr()->neq("u.user", "l.userTwo")
              )
            )
            ->setParameter("thisUser", $this->getUser())
            ->getQuery()->getResult();*/
            $userFavourites = $query
                ->select(array("u"))
                ->from("RFSAppBundle:UserAccommodationInterest", "u")
                ->from("RFSAppBundle:LinkedUser", "l")
                ->where(
                  $query->expr()->andX(
                    $query->expr()->neq("u.user", ":thisUser"),
                    $query->expr()->andX(
                      $query->expr()->andX(
                          $query->expr()->eq("l.userOne", ":thisUser"),
                          $query->expr()->neq("u.user", "l.userTwo")
                      ),
                      $query->expr()->andX(
                        $query->expr()->eq("l.userTwo", ":thisUser"),
                        $query->expr()->neq("u.user", "l.userOne")
                      )
                    )
                  )
                )
                ->setParameter("thisUser", $this->getUser())
                ->getQuery()->getResult();


        return array("property" => $property, "linkedUsersOne" => $linkedUsersOne, "linkedUsersTwo" => $linkedUsersTwo, "userFavourites" => $userFavourites);
  }

  /**
   * @Route("/users/execute", name="share_via_user_execute")
   *
   */
  public function shareUserExecuteAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();

        //find property that is about to be shared
        $property = $em->getRepository("RFSAppBundle:Accommodation")->findOneById($request->get("property_id"));

        if($property == NULL)
        {
            //if property does not exist then go back to the share screen
            $session = $request->getSession();
            $propertyId = $session->get("property_id");

            if($propertyId == NULL)
            {
                $propertyId = 116;
            }

            return $this->redirect("/share/user/" . $propertyId);
        }

        $user = $this->getUser();

        //find lists of people that they were able to share the property with
        $linkedUsersOne = $em->getRepository("RFSAppBundle:LinkedUser")->findByUserOne($user);
        $linkedUsersTwo = $em->getRepository("RFSAppBundle:LinkedUser")->findByUserTwo($user);
        $userFavourites = $query
            ->select(array("u"))
            ->from("RFSAppBundle:UserAccommodationInterest", "u")
            ->from("RFSAppBundle:LinkedUser", "l")
            ->where(
              $query->expr()->andX(
                $query->expr()->neq("u.user", ":thisUser"),
                $query->expr()->andX(
                  $query->expr()->andX(
                      $query->expr()->eq("l.userOne", ":thisUser"),
                      $query->expr()->neq("u.user", "l.userTwo")
                  ),
                  $query->expr()->andX(
                    $query->expr()->eq("l.userTwo", ":thisUser"),
                    $query->expr()->neq("u.user", "l.userOne")
                  )
                )
              )
            )
            ->setParameter("thisUser", $this->getUser())
            ->getQuery()->getResult();

        //find the users that had been ticked in the previous form
        $allSelectedUsers = array();

        //list one
        for($i = 0; $i < sizeof($linkedUsersOne); $i++)
        {
            if($request->get("friend_list_a" . $i) == "on")
            {
                //friend was selected add to selected array
                //$selectedUsersOne[sizeof($selectedUsersOne)] = $linkedUsersOne[$i];
                $allSelectedUsers[sizeof($allSelectedUsers)] = $linkedUsersOne[$i]->getUserTwo();
            }
        }

        //list two
        for($i = 0; $i < sizeof($linkedUsersTwo); $i++)
        {
            if($request->get("friend_list_b" . $i) == "on")
            {
                $allSelectedUsers[sizeof($allSelectedUsers)] = $linkedUsersTwo[$i]->getUserOne();
            }
        }

        //list three
        $selectedFavouriteUsers = array();  //third list of users to share message with
        for($i = 0; $i < sizeof($userFavourites); $i++)
        {
          if($request->get("user_favourite" . $i) == "on")
          {
            $allSelectedUsers[sizeof($allSelectedUsers)] = $userFavourites[$i]->getUser();
          }
        }

        //produce a userShares object
        $shareMessage = new Share();
        $shareMessage->setUser($user);
        $shareMessage->setAccommodation($property);
        $shareMessage->setMessage($request->get("form_message"));
        $shareMessage->setLandlordContact(false);

        //persist and flush shareMessage to database getting back an id for the object
        $em->persist($shareMessage);
        $em->flush();

        //produce individual links to that share message for each user involved
        for($i = 0; $i < sizeOf($selectedUsersOne); $i++)
        {
            $messageLink = new ShareUser();
            $messageLink->setUser($allSelectedUsers[$i]);
            $messageLink->setShare($shareMessage);
            $messageLink->setHasSeen(false);

            $em->persist($messageLink);
        }

        $em->flush();

        return new Response("<script>window.close()</script>");
  }

  /**
   * @Route("/find/friends/{landlordContact}", name="share_find_friends")
   * @Template("RFSAppBundle:Sharing:find-friends.html.twig")
   */
  public function findFriendsAction(Request $request, $landlordContact)
  {


        $session = $request->getSession();
        $currentProperty = $session->get("property_id");

        if($currentProperty == NULL)
        {
            $currentProperty = 117;
        }

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        if($request->get("email_search"))
        {
            $email = $request->get("email_search");
            $em = $this->getDoctrine()->getManager();

            $friend = $em->getRepository("ApplicationSonataUserBundle:User")->findOneByEmail($request->get("email_search"));

            if($friend == NULL || $friend == $this->getUser())
            {
                return array("found" => false, "landlordContact" => $landlordContact);
            }

            return array("found" => true, "friendUser" => $friend, "propertyId" => $currentProperty, "landlordContact" => $landlordContact);
        }

        return array("propertyId" => $currentProperty, "landlordContact" => $landlordContact);
  }

  /**
   * @Route("/friends/link/{landlordContact}", name="share_friends_link")
   * @Template("RFSAppBundle:Sharing:find-friends.html.twig")
   */
  public function linkFriendsAction(Request $request, $landlordContact)
  {
        $session = $request->getSession();
        $currentProperty = $session->get("property_id");

        if($currentProperty == NULL)
        {
            $currentProperty = 117;
        }

        if($request->get("friend_email"))
        {
            $email = $request->get("friend_email");
            $em = $this->getDoctrine()->getManager();

            $friend = $em->getRepository("ApplicationSonataUserBundle:User")->findOneByUsername($email);

            if($friend == NULL)
            {
                //return new Response("friend does not exist");
                return  array("propertyId" => $currentProperty, "landlordContact" => $landlordContact);
            }

            $myUser = $this->getUser();

            if($friend == $myUser)
            {
                //return new Response("friend was the same as user");
                return array("message" => "You cannot add yourself", "propertyId" => $currentProperty, "landlordContact" => $landlordContact);
            }

            $currentLinkedUsers = $em->getRepository("RFSAppBundle:LinkedUser")->findOneBy(array("userOne" => $myUser, "userTwo" => $friend));
            if($currentLinkedUsers == NULL)
            {
                $currentLinkedUsers = $em->getRepository("RFSAppBundle:LinkedUser")->findOneBy(array("userOne" => $friend, "userTwo" => $myUser));
                if($currentLinkedUsers != NULL)
                {
                    //return new Response("FAILED");
                    //return $this->render("/groups/find=friends-form.html.twig");
                    return array("message" => "You already have this user added", "propertyId" => $currentProperty, "landlordContact" => $landlordContact);
                }
            }
            else
            {
                //return new Response("FAILED");
                return array("message" => "You already have this user added", "propertyId" => $currentProperty, "landlordContact" => $landlordContact);
            }

            $linkedUserObject = new LinkedUser();
            $linkedUserObject->setUserOne($myUser);
            $linkedUserObject->setUserTwo($friend);

            $em->persist($linkedUserObject);
            $em->flush();

            return array("message" => "User successfully added", "propertyId" => $currentProperty, "landlordContact" => $landlordContact);
          }
    }
}
