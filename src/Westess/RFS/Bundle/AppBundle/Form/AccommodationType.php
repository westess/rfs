<?php

namespace Westess\RFS\Bundle\AppBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Westess\RFS\Bundle\AppBundle\Resources\Upload;

class AccommodationType extends AbstractType
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choiceArray = array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9);
        $builder
            ->add('description', 'textarea')
            ->add('price', 'money', array('divisor' => 1, 'currency' => 'GBP'))
            ->add('pricePlan', 'choice', array(
                'choices' => array(1 => 'Weekly', 2 => 'Monthly', 3 => 'Yearly'),
                'multiple' => false))
            ->add('contractLength')
            ->add('bedrooms', 'choice', array('choices' => $choiceArray, 'multiple' => false))
            ->add('isEnSuite', 'choice', array(
                'label' => 'Bathroom','choices' => array(1 => 'En suite', 0 => 'Shared'), 'multiple' => false))
            ->add('fullAddress')
            ->add('postcode', 'hidden')
            ->add('latitude', 'hidden')
            ->add('longitude', 'hidden')
            ->add('isVisible', 'choice', array(
                'label' => 'Visibility', 'choices' => array(0 => "hidden", 1 => 'visible'),
                'placeholder' => false, 'expanded' => true, 'multiple' => false))
            ->add('publishStartDate', 'date', array('html5' => true, 'widget' => 'single_text', 'required' => false, 'attr' => array('class' => '')))
            ->add('publishEndDate', 'date', array('html5' => true, 'widget' => 'single_text', 'required' => false, 'attr' => array('class' => '')))
            ->add('accommodationType', 'entity', array('class' => 'Westess\RFS\Bundle\AppBundle\Entity\AccommodationType', 'property' => 'name'))
            ->add('mainPhoto')
            ->add('features', 'collection', array('label' => false, 'type' => new FeatureType(),
                'allow_add' => true, 'by_reference' => false));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $accommodation = $event->getData();
            $form = $event->getForm();
            if ($accommodation && is_null($accommodation->getId())) {
                $form
                    ->add('mainPhoto', 'file')
                    ->add('photos', 'collection', array(
                        'label' => False,
                        'type' => 'file',
                        'allow_add' => true,
                        'options' => array(
                            'required' => false,
                            'attr' => array('class' => ''))));
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            if ('POST' == $event->getForm()->getConfig()->getMethod()) {

                $data = $event->getData();

                // handle new mainPhoto upload
                $mainPhoto = Upload::uploadImage($data['mainPhoto'], $this->em);
                $data['mainPhoto'] = $mainPhoto;

                // handle other photos
                if (isset($data['photos'])) {
                    $photos = array();
                    foreach ($data['photos'] as $photo) {
                        $photos[] = Upload::uploadImage($photo, $this->em);
                    }
                    $data['photos'] = $photos;
                }

                $event->setData($data);
            } elseif ('PUT' == $event->getForm()->getConfig()->getMethod()) {
                // handle edit property photo uploads
            }
        });

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Westess\RFS\Bundle\AppBundle\Entity\Accommodation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rfs_accommodation';
    }
}
