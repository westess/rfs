<?php

namespace Westess\RFS\Bundle\AppBundle\Resources;

use Doctrine\Entity;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use  Westess\RFS\Bundle\AppBundle\Entity\Photo;


class Upload
{
    /**
     * @param UploadedFile $image
     * @param EntityManager $em
     * @return Photo
     */
    public static function uploadImage(UploadedFile $image, EntityManager $em)
    {
        $photo = new Photo();
        if ($image->getError() == '0') {
            $fileName = preg_replace('#[^a-z.0-9]#i', '', $image->getClientOriginalName());
            $tmp = explode(".", $fileName);
            $fileExt = end($tmp);
            if (in_array(strtolower($fileExt), array('jpg', 'jpeg', 'bmp', 'gif', 'png'))) {
                $photo->setFile($image);
                $photo->setVisible(1);
                $em->persist($photo);
                $em->flush();

                //convert to jpg
                if (strtolower($fileExt) != "jpg") {
                    $tmp = $photo->getAppPath();
                    @self::img_convert_to_jpg($tmp, $tmp . 'tmp', $fileExt);
                    if (!rename($tmp . 'tmp', $tmp)) {
                        throw new FileException("File upload error contact developer: cannot convert image to jpg");
                    }
                }

                $image = new ZebraImage();
                $image->source_path = $image->target_path = $photo->getAppPath();

                if (!$image->resize(1200, 800, ZEBRA_IMAGE_CROP_CENTER)) {
                    throw new FileException("File upload error contact developer: cannot resize image");
                }

                $photo->saveToBucket();

            } else {
                throw new FileException("File upload error contact developer: cannot upload '." . $fileExt . "' files");
            }
        }
        return $photo;
    }

    /**
     * @param UploadedFile $image
     * @param Photo $photo
     */
    public static function updateImage(UploadedFile $image, Photo $photo)
    {
        if ($image->getError() == '0') {
            $fileName = preg_replace('#[^a-z.0-9]#i', '', $image->getClientOriginalName());
            $tmp = explode(".", $fileName);
            $fileExt = end($tmp);
            if (in_array(strtolower($fileExt), array('jpg', 'jpeg', 'bmp', 'gif', 'png'))) {
                $photo->setFile($image);
                $photo->upload();

                //convert to jpg
                if (strtolower($fileExt) != "jpg") {
                    $tmp = $photo->getAppPath();
                    @self::img_convert_to_jpg($tmp, $tmp . 'tmp', $fileExt);
                    if (!rename($tmp . 'tmp', $tmp)) {
                        throw new FileException("File upload error contact developer: cannot convert image to jpg");
                    }
                }

                $image = new ZebraImage();
                $image->source_path = $image->target_path = $photo->getAppPath();

                if (!$image->resize(1200, 800, ZEBRA_IMAGE_CROP_CENTER)) {
                    throw new FileException("File upload error contact developer: cannot resize image");
                }

                $photo->saveToBucket();

            } else {
                throw new FileException("File upload error contact developer: cannot upload '." . $fileExt . "' files");
            }
        }
    }

    /**
     * The following scripts are inspired by Adam Khoury's PHP Image Function Library 1.0
     * Function for converting GIFs and PNGs to JPG upon upload
     */
    public static function img_convert_to_jpg($target, $newCopy, $ext)
    {
        list($w_orig, $h_orig) = getimagesize($target);
        $ext = strtolower($ext);
        $img = "";
        if ($ext == "gif") {
            $img = imagecreatefromgif($target);
        } else if ($ext == "png") {
            $img = imagecreatefrompng($target);
        }
        $tci = imagecreatetruecolor($w_orig, $h_orig);
        imagecopyresampled($tci, $img, 0, 0, 0, 0, $w_orig, $h_orig, $w_orig, $h_orig);
        imagejpeg($tci, $newCopy, 80);
    }

}

?>