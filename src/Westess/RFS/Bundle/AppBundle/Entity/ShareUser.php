<?php

namespace Westess\RFS\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShareUser
 */
class ShareUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $hasSeen;

    /**
     * @var \Westess\RFS\Bundle\AppBundle\Entity\Share
     */
    private $share;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hasSeen
     *
     * @param boolean $hasSeen
     * @return ShareUser
     */
    public function setHasSeen($hasSeen)
    {
        $this->hasSeen = $hasSeen;

        return $this;
    }

    /**
     * Get hasSeen
     *
     * @return boolean 
     */
    public function getHasSeen()
    {
        return $this->hasSeen;
    }

    /**
     * Set share
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Share $share
     * @return ShareUser
     */
    public function setShare(\Westess\RFS\Bundle\AppBundle\Entity\Share $share = null)
    {
        $this->share = $share;

        return $this;
    }

    /**
     * Get share
     *
     * @return \Westess\RFS\Bundle\AppBundle\Entity\Share 
     */
    public function getShare()
    {
        return $this->share;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return ShareUser
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
