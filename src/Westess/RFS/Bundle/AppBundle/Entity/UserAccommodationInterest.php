<?php

namespace Westess\RFS\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAccommodationInterest
 */
class UserAccommodationInterest
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \Westess\RFS\Bundle\AppBundle\Entity\Accommodation
     */
    private $accommodation;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return UserAccommodationInterest
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set accommodation
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Accommodation $accommodation
     * @return UserAccommodationInterest
     */
    public function setAccommodation(\Westess\RFS\Bundle\AppBundle\Entity\Accommodation $accommodation = null)
    {
        $this->accommodation = $accommodation;

        return $this;
    }

    /**
     * Get accommodation
     *
     * @return \Westess\RFS\Bundle\AppBundle\Entity\Accommodation 
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }
}
