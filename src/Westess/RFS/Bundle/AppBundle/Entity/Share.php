<?php

namespace Westess\RFS\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Share
 */
class Share
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \Westess\RFS\Bundle\AppBundle\Entity\Accommodation
     */
    private $accommodation;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Share
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set accommodation
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Accommodation $accommodation
     * @return Share
     */
    public function setAccommodation(\Westess\RFS\Bundle\AppBundle\Entity\Accommodation $accommodation = null)
    {
        $this->accommodation = $accommodation;

        return $this;
    }

    /**
     * Get accommodation
     *
     * @return \Westess\RFS\Bundle\AppBundle\Entity\Accommodation 
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Share
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var boolean
     */
    private $landlordContact;


    /**
     * Set landlordContact
     *
     * @param boolean $landlordContact
     * @return Share
     */
    public function setLandlordContact($landlordContact)
    {
        $this->landlordContact = $landlordContact;

        return $this;
    }

    /**
     * Get landlordContact
     *
     * @return boolean 
     */
    public function getLandlordContact()
    {
        return $this->landlordContact;
    }
}
