<?php

namespace Westess\RFS\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LinkedUser
 */
class LinkedUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $userTwo;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $userOne;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userTwo
     *
     * @param \Application\Sonata\UserBundle\Entity\User $userTwo
     * @return LinkedUser
     */
    public function setUserTwo(\Application\Sonata\UserBundle\Entity\User $userTwo = null)
    {
        $this->userTwo = $userTwo;

        return $this;
    }

    /**
     * Get userTwo
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUserTwo()
    {
        return $this->userTwo;
    }

    /**
     * Set userOne
     *
     * @param \Application\Sonata\UserBundle\Entity\User $userOne
     * @return LinkedUser
     */
    public function setUserOne(\Application\Sonata\UserBundle\Entity\User $userOne = null)
    {
        $this->userOne = $userOne;

        return $this;
    }

    /**
     * Get userOne
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUserOne()
    {
        return $this->userOne;
    }
}
