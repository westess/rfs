var filterContainer= $('#filter-container');
var map = $('#map');
var accommodationPanels = $('#accommodation-panels');
var accommodationPage = $('#accommodation-page');
var loading = $('#loading');

function openAccommodation(url){
    accommodationPage.attr('src', url);
    filterContainer.hide();
    accommodationPanels.hide();
    loading.show();
}

function closeAccommodation(){
    accommodationPage.hide();
    // destroy iframe content
    frameDoc = accommodationPage[0].contentDocument || accommodationPage[0].contentWindow.document;
    frameDoc.removeChild(frameDoc.documentElement);
    filterContainer.show();
    accommodationPanels.show();
    loading.hide();
}

var priceSlider = $("#price-slider");
var priceField = $("#price");
priceSlider.slider({
    range: true,
    values: [50, 500],
    min: 0,
    max: 1000,
    step: 50,
    slide: function (event, ui) {
        priceField.text("£" + ui.values[0] + " - $" + ui.values[1]);
    }
});
priceField.text("£" + priceSlider.slider("values", 0) + " - £" + priceSlider.slider("values", 1));

var getPricePlan = function (id) {
    switch (id) {
        case 1:
            return "week";
            break;
        case 2:
            return "month";
            break;
        case 3:
            return "year";
            break;
    }
};

var getPhotoWebPath = function (id) {
    var url = "https://s3-eu-west-1.amazonaws.com/rfs-assets/uploads/images/accommodation";
    return url + '/' + id + '/' + id + '.jpg';
}

var shouldZoomOut = false;
var zoomBackToUniversity = function () {
    if (shouldZoomOut) {
        mapGLOBAL.panTo(uniLatLng);
        mapGLOBAL.setZoom(defaultZoom);
    }
}

var exciteMarker = function (accommodation) {
    var latLng = new google.maps.LatLng(accommodation.latitude, accommodation.longitude);
    return function () {
        shouldZoomOut = false;
        mapGLOBAL.panTo(latLng);
        mapGLOBAL.setZoom(15);
        var el = $('.accommodation-marker-' + accommodation.id)[0];
        $(el).css('background', primaryColor);
    }
};
var depressMarker = function (accommodation) {
    return function () {
        var el = $('.accommodation-marker-' + accommodation.id)[0];
        $(el).css('background', 'white');
        shouldZoomOut = true;
        setTimeout("zoomBackToUniversity()", 1000);
    }
};

var locations = new Array();
var contents = new Array();
var ids = new Array();
var images = new Array();

_.each(accommodations, function (accommodation, index) {
    var type = accommodation.accommodation_type.name;
    var photo = getPhotoWebPath(accommodation.id);
    var price = accommodation.price;
    var address = accommodation.full_address.substr(0, 25);
    var link = '/accommodation/' + accommodation.id;

    locations.push([accommodation.latitude, accommodation.longitude]);

    contents.push('<div class="infobox"><div class="infobox-header"><h3 class="infobox-title">' +
    '<a href="javascript:openAccommodation(\'' + link + '\');">' + address + '</a></h3><h4 class="infobox-subtitle">' +
    '<a href="javascript:openAccommodation(\'' + link + '\');">' + type + '</a></h4></div><div class="infobox-picture">' +
    '<a href="javascript:openAccommodation(\'' + link + '\');"><img src="' + photo + '"></a>' +
    '<div class="infobox-price">£' + price + '</div></div></div>');

    ids.push("accommodation-marker-" + accommodation.id);
    images.push('<img src="/img/icons/' + type + '.png" alt="">');

    var template = _.template($('#accommodation-panel-template').html());
    var el = $(template(accommodation));
    el.hover(exciteMarker(accommodation), depressMarker(accommodation));
    accommodationPanels.append(el);
});

loading.hide();

var infowindow;

//Create array of available interests
var interests = ["None", "Churhes", "Gyms", "Mosques", "Stores", "Synagogues", "Temples"];

var renderInterestPoints = function (interest) {
    removeInterestMarkers();
    var request = null;
    switch (interest) {
        case interests[1]:
            request = {
                location: uniLatLng,
                radius: 2500,
                types: ['church']
            }
            break;
        case interests[2]:
            request = {
                location: uniLatLng,
                radius: 2500,
                types: ['gym']
            }
            break;
        case interests[3]:
            request = {
                location: uniLatLng,
                radius: 2500,
                types: ['mosque']
            }
            break;
        case interests[4]:
            request = {
                location: uniLatLng,
                radius: 2500,
                types: ['store'],
                name: "asda|tesco|morrisons|sainsbury"
            };
            break;
        case interests[5]:
            request = {
                location: uniLatLng,
                radius: 2500,
                types: ['synagogue']
            }
            break;
        case interests[6]:
            request = {
                location: uniLatLng,
                radius: 2500,
                types: ['hindu_temple']
            }
    }
    if (request !== null) {
        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, pointsOfInterestCallback);
    }
}

var pointsOfInterestCallback = function (results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            createPointOfInterestMarker(results[i]);
        }
    }
}

var interestMarkers = [];

var createPointOfInterestMarker = function (place) {
    var placeLoc = place.geometry.location;
    interestMarkers.push(new google.maps.Marker({
            map: map,
            position: placeLoc
        })
    );
    google.maps.event.addListener(_.last(interestMarkers), 'click', function () {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
    })
}

var removeInterestMarkers = function () {
    for (var i = 0; i < interestMarkers.length; i++) {
        interestMarkers[i].setMap(null);
    }
    interestMarkers = [];
}

//Create interests select list
var interestLabel = document.createTextNode('Interests:  ');
var interestSelectList = document.createElement("select");
interestSelectList.id = "interests";

//Create and append the options
for (var i = 0; i < interests.length; i++) {
    var option = document.createElement("option");
    option.value = interests[i];
    option.text = interests[i];
    interestSelectList.appendChild(option);
}
//Add change event listener
interestSelectList.addEventListener('change', function () {
    var interest = interestSelectList.options[interestSelectList.selectedIndex].value;
    renderInterestPoints(interest);
});

var addInterestToMap = function () {
    var interest = $('#map div.gm-style-mtc div')[0];
    interest.removeChild(interest.childNodes[0]);
    interest.appendChild(interestLabel);
    interest.appendChild(interestSelectList);
}

$(document).ready(function () {
    map.aviators_map({
        locations: locations,
        contents: contents,
        types: ids,
        images: images,
        transparentMarkerImage: '/img/marker-transparent.png',
        transparentClusterImage: '/img/clusterer-transparent.png',
        zoom: defaultZoom,
        center: {
            latitude: uniLat,
            longitude: uniLon
        },
        enableGeolocation: false,
        styles: styles[7],
        drawCircle: true
    });

    google.maps.event.addDomListener(window, 'load', setTimeout(addInterestToMap, 5000));

    var pricePlan = $('#price_plan');
    var contractLength = $('#contract_length');
    var priceSlider = $("#price-slider");
    var accommodationType = $('#accommodation_type');
    var isEnSuite = $('#is_en_suite');
    var bedrooms = $('#bedrooms');
    var clearFilter = $('#clear-filter');
    var applyFilter = $('#apply-filter');

    var priceField = $("#price");
    priceSlider.slider({
        range: true,
        values: [50, 500],
        min: 0,
        max: 1000,
        step: 50,
        slide: function (event, ui) {
            priceField.text("£" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    priceField.text("£" + priceSlider.slider("values", 0) + " - £" + priceSlider.slider("values", 1));

    var showAccommodations = function (accommodations) {
        _.each(accommodations, function (accommodation, index) {
            $('.accommodation-marker-' + accommodation.id).show();
            $('#accommodation-panel-' + accommodation.id).show();
        });
    }

    var hideAccommodations = function (accommodations) {
        _.each(accommodations, function (accommodation, index) {
            $('.accommodation-marker-' + accommodation.id).hide();
            $('#accommodation-panel-' + accommodation.id).hide();
        });
    }

    clearFilter.click(function (ev) {
        showAccommodations(accommodations);
        pricePlan.val("Any");
        accommodationType.val("Any");
        isEnSuite.val("Any");
    });

    applyFilter.click(function (ev) {
        var filteredAccommodations = _.filter(accommodations, function (accommodation) {
            var priceQuery = priceSlider.slider("values", 0) <= accommodation.price && accommodation.price <= priceSlider.slider("values", 1);
            var pricePlanQuery = pricePlan.val() === "Any" ? true : accommodation.price_plan === parseInt(pricePlan.val());
            var contractLengthQuery = contractLength.val() === "Any" ? true : accommodation.contract_length === parseInt(contractLength.val());
            var accommodationTypeQuery = accommodationType.val() === "Any" ? true : accommodation.accommodation_type.name === accommodationType.val();
            var bedroomsQuery = bedrooms.val() === "Any" ? true : accommodation.bedrooms === parseInt(bedrooms.val());
            var isEnSuiteQuery = isEnSuite.val() === "Any" ? true : accommodation.is_en_suite === (isEnSuite.val() === "true");

            return priceQuery && pricePlanQuery && contractLengthQuery && accommodationTypeQuery && bedroomsQuery && isEnSuiteQuery;
        });
        showAccommodations(filteredAccommodations);
        hideAccommodations(_.difference(accommodations, filteredAccommodations));
    });

    var weeklyContracts = '<option value="Any">Any</option><option value="1">4 weeks</option><option value="2">8 weeks</option><option value="3">12 weeks</option><option value="4">16 weeks</option><option value="5">20 weeks</option><option value="6">24 weeks</option>';
    var monthlyContracts = '<option value="Any">Any</option><option value="1">1 months</option><option value="2">2 months</option><option value="3">3 months</option><option value="4">4 months</option><option value="5">5 months</option><option value="6">6 months</option>';
    var yearlyContracts = '<option value="Any">Any</option><option value="1">1/4 year</option><option value="2">1/2 year</option><option value="3">1 year</option>';

    pricePlan.change(function () {
        switch (pricePlan.val()) {
            case "Any":
                contractLength.val("Any");
                disable(contractLength);
                break;
            case "1":
                enable(contractLength);
                contractLength.html(weeklyContracts);
                break;
            case "2":
                enable(contractLength);
                contractLength.html(monthlyContracts);
                break;
            case "3":
                enable(contractLength);
                contractLength.html(yearlyContracts);
                break;
        }
    });

    accommodationType.change(function () {
        switch (accommodationType.val()) {
            case "Any":
                enable(bedrooms);
                bedrooms.val("Any");
                break;
            case "Room":
                bedrooms.val(1);
                disable(bedrooms);
                break;
            case "House":
                enable(bedrooms);
                bedrooms.val("Any");
                break;
            case "Studio":
                bedrooms.val(1);
                disable(bedrooms);
                break;
            case "Flat":
                enable(bedrooms);
                bedrooms.val("Any");
                break;
        }
    });

    var enable = function (el) {
        el.prop("disabled", false)
    };
    var disable = function (el) {
        el.prop("disabled", true)
    }

});