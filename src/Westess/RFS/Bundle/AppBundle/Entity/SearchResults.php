<?php
namespace Westess\RFS\Bundle\AppBundle\Entity;

class SearchResults
{
  private $minimumPrice;
  private $maximumPrice;
  private $accommodationType;
  private $pricePlan;
  private $contractLength;
  private $bedrooms;
  private $ensuite;

  function __construct($minimumPrice, $maximumPrice, $accommodationType, $pricePlan, $contractLength, $bedrooms, $ensuite)
  {
    $this->minimumPrice = $minimumPrice;
    $this->maximumPrice = $maximumPrice;
    $this->accommodationType = $accommodationType;
    $this->pricePlan = $pricePlan;
    $this->contractLength = $contractLength;
    $this->bedrooms = $bedrooms;
    $this->ensuite = $ensuite;
  }

  public function getMinimumPrice()
  {
    return $this->minimumPrice;
  }
  public function getMaximumPrice()
  {
    return $this->maximumPrice;
  }
  public function getAccommodationType()
  {
    return $this->accommodationType;
  }
  public function getPricePlan()
  {
    return $this->pricePlan;
  }
  public function getContractLength()
  {
    return $this->contractLength;
  }
  public function getBedrooms()
  {
    return $this->bedrooms;
  }
  public function isEnsuite()
  {
    return $this->ensuite;
  }

  public function setMinimumPrice($minimumPrice)
  {
    $this->minimumPrice = $minimumPrice;
  }
  public function setMaximumPrice($maximumPrice)
  {
    $this->maximumPrice = $maximumPrice;
  }
  public function setAccommodationType($accommodationType)
  {
    $this->accommodationType = $accommodationType;
  }
  public function setPricePlan($pricePlan)
  {
    $this->pricePlan = $pricePlan;
  }
  public function setContractLength($contractLength)
  {
    $this->contractLength = $contractLength;
  }
  public function setBedrooms($bedrooms)
  {
    $this->bedrooms = $bedrooms;
  }
  public function setEnsuite($ensuite)
  {
    $this->ensuite = $ensuite;
  }
}
