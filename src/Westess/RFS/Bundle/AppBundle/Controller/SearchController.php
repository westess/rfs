<?php

namespace Westess\RFS\Bundle\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Westess\RFS\Bundle\AppBundle\Entity\SearchResults;

class SearchController extends Controller
{
    /**
     * @param $uniCode
     * @return array|bool (array of accommodations near uni | false if uni does not exist)
     */
    private function getAccommodationsNearUni($uniCode)
    {
        $em = $this->getDoctrine()->getManager();
        $university = $em->getRepository("RFSAppBundle:University")->findOneByCode($uniCode);  //collect the chosen university

        if ($university == NULL) {
            //failed to find the chosen university
            return false;
        }

        //university was found
        //create the query for finding the properties in this postcode area
        $query = $em->createQueryBuilder();

        //return array of properties in this postcode
        $accommodations = $query
            ->select(array("a"))
            ->from("RFSAppBundle:Accommodation", "a")
            ->where($query->expr()->andX(
                $query->expr()->like("a.postcode", ":uniPostcode"),
                $query->expr()->eq("a.isVisible", ":visibility")))
            ->setParameter("uniPostcode", $university->getPostcode() . "%")
            ->setParameter("visibility", true)
            ->getQuery()->getResult();

        //shortlist properties to within one mile radius
        $accommodationsShortlist = array();
        for ($i = 0; $i < sizeof($accommodations); $i++) {
            if (self::isAccommodationInRadius($accommodations[$i], $university, 1)) {
                $accommodationsShortlist[sizeof($accommodationsShortlist)] = $accommodations[$i];
            }
        }

        return $accommodationsShortlist;
    }

    /**
     * @param $accommodation
     * @param $university
     * @param $maxDistance
     * @return bool (true if the given property is within the given distance   (maxDistance in miles))
     */
    private function isAccommodationInRadius($accommodation, $university, $maxDistance)
    {
        $uniLat = $university->getLatitude();
        $uniLong = $university->getLongitude();

        $propLat = $accommodation->getLatitude();
        $propLong = $accommodation->getLongitude();

        //convert locations to radians
        $propLat = deg2rad($propLat);
        $propLong = deg2rad($propLong);
        $uniLat = deg2rad($uniLat);
        $uniLong = deg2rad($uniLong);

        $a = pow(sin(($propLat - $uniLat) / 2), 2) + cos($uniLat) * cos($propLat) * pow(sin(($propLong - $uniLong) / 2), 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        $distance = $c * 6371000;   //6371000 is the radius of the earth in metres

        $distance = $distance * 0.000621371192; //0.000621371192 is the number of metres in a mile  (converts distance to miles)

        if ($distance < $maxDistance) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @Route("/search/{uni}", name="search")
     * @param Request $request
     * @param $uni
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request, $uni)
    {
        $accommodations = self::getAccommodationsNearUni($uni);
        $serializer = $this->get('jms_serializer');
        $accommodations = $serializer->serialize($accommodations, 'json');

        $em = $this->getDoctrine()->getManager();
        $university = $em->getRepository("RFSAppBundle:University")->findOneByCode($uni);

        return $this->render('RFSAppBundle:Default:search.html.twig', array('accommodations' => $accommodations, 'university' => $university));
    }
}
