<?php
//git check
namespace Westess\RFS\Bundle\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Westess\RFS\Bundle\AppBundle\Entity\Accommodation;
use Application\Sonata\UserBundle\Entity\User;


class LandlordController extends Controller
{
  /**
   * @Route("/landlord/contact/{id}", name="landlord_landlord_popup")
   * @Method("GET");
   * @Template("RFSAppBundle:Sharing:contact-landlord-popup.html.twig")
   */
   function landlordContactAction(Request $request, $id)
   {
     $em = $this->getDoctrine()->getManager();
     $accommodation = $em->getRepository("RFSAppBundle:Accommodation")->findOneById($id);

     $session = $request->getSession();

     $session->set("property_id", $id);

     if(!$this->get('security.authorization_checker')->isGranted("ROLE_USER"))
     {
       $session->getFlashBag()->add("info", "You must be logged in to use this feature");
       throw $this->createAccessDeniedException();
     }

     $em = $this->getDoctrine()->getManager();
     $property = $em->getRepository("RFSAppBundle:Accommodation")->findOneById($id);

     //$user = new User();
     //$user = $this->getUser()->getId();

     $user = $this->getUser();

     $linkedUsersOne = $em->getRepository("RFSAppBundle:LinkedUser")->findByUserOne($user);
     $linkedUsersTwo = $em->getRepository("RFSAppBundle:LinkedUser")->findByUserTwo($user);

     return array("property" => $accommodation, "linkedUsersOne" => $linkedUsersOne, "linkedUsersTwo" => $linkedUsersTwo);
   }

   /**
    * @Route("/landlord/contact/execute")
    */
    function landlordContactExecuteAction(Request $request)
    {
      if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
          $session = $request->getSession();
          $session->getFlashBag()->add('info', 'You must be logged in to use this feature');
          throw $this->createAccessDeniedException();
      }
      $em = $this->getDoctrine()->getManager();

          //find property that is about to be shared
          $property = $em->getRepository("RFSAppBundle:Accommodation")->findOneById($request->get("property_id"));

          if($property == NULL)
          {
              //if property does not exist then go back to the share screen
              $session = $request->getSession();
              $propertyId = $session->get("property_id");

              if($propertyId == NULL)
              {
                  $propertyId = 116;
              }

              return $this->redirect("/landlord/contact/" . $propertyId);
          }

          $user = $this->getUser();

          //find lists of people that they were able to share the property with
          $linkedUsersOne = $em->getRepository("RFSAppBundle:LinkedUser")->findByUserOne($user);
          $linkedUsersTwo = $em->getRepository("RFSAppBundle:LinkedUser")->findByUserTwo($user);

          //find the users that had been ticked in the previous form
          //list one
          $selectedUsersOne = array();  //first list of users to share message with
          for($i = 0; $i < sizeof($linkedUsersOne); $i++)
          {
              if($request->get("friend_list_a" . $i) == "on")
              {
                  //friend was selected add to selected array
                  $selectedUsersOne[sizeof($selectedUsersOne)] = $linkedUsersOne[$i];
              }
          }

          //list two
          $selectedUsersTwo = array(); //second list of users to share message with
          for($i = 0; $i < sizeof($linkedUsersTwo); $i++)
          {
              if($request->get("friend_list_b" . $i) == "on")
              {
                  //friend was selected add to selected array
                  $selectedUsersTwo[sizeof($selectedUsersTwo)] = $linkedUsersTwo[$i];
              }
          }

          //return new Response($selectedUsersOne[1]->getUserTwo()->getUsername());
          //return new Response(sizeof($selectedUsersOne));


          //produce a userShares object
          $shareMessage = new Share();
          $shareMessage->setUser($user);
          $shareMessage->setAccommodation($property);
          $shareMessage->setMessage($request->get("form_message"));
          $shareMessage->setLandlordContact(true);

          //persist and flush shareMessage to database getting back an id for the object
          $em->persist($shareMessage);
          $em->flush();

          //produce individual links to that share message for each user involved
          //list one
          for($i = 0; $i < sizeOf($selectedUsersOne); $i++)
          {
              $messageLink = new ShareUser();
              $messageLink->setUser($selectedUsersOne[$i]->getUserTwo());
              $messageLink->setShare($shareMessage);
              $messageLink->setHasSeen(false);

              $em->persist($messageLink);
          }

          //list two
          for($i = 0; $i < sizeOf($selectedUsersTwo); $i++)
          {
            $messageLink = new ShareUser();
            $messageLink->setUser($selectedUsersTwo[$i]->getUserOne());
            $messageLink->setShare($shareMessage);
            $messageLink->setHasSeen(false);

            $em->persist($messageLink);
          }

          $em->flush();

          return new Response("<script>window.close()</script>");
    }
}
