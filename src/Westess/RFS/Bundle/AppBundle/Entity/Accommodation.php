<?php

namespace Westess\RFS\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accommodation
 */
class Accommodation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $price;

    /**
     * @var integer
     */
    private $pricePlan;

    /**
     * @var integer
     */
    private $contractLength;

    /**
     * @var integer
     */
    private $bedrooms;

    /**
     * @var boolean
     */
    private $isEnSuite;

    /**
     * @var string
     */
    private $fullAddress;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @var boolean
     */
    private $isVisible;

    /**
     * @var boolean
     */
    private $isFeatured;

    /**
     * @var boolean
     */
    private $isPublishable;

    /**
     * @var \DateTime
     */
    private $publishStartDate;

    /**
     * @var \DateTime
     */
    private $publishEndDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $features;

    /**
     * @var \Westess\RFS\Bundle\AppBundle\Entity\AccommodationType
     */
    private $accommodationType;

    /**
     * @var \Westess\RFS\Bundle\AppBundle\Entity\Photo
     */
    private $mainPhoto;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->features = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Accommodation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Accommodation
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set pricePlan
     *
     * @param integer $pricePlan
     * @return Accommodation
     */
    public function setPricePlan($pricePlan)
    {
        $this->pricePlan = $pricePlan;

        return $this;
    }

    /**
     * Get pricePlan
     *
     * @return integer 
     */
    public function getPricePlan()
    {
        return $this->pricePlan;
    }

    /**
     * Set contractLength
     *
     * @param integer $contractLength
     * @return Accommodation
     */
    public function setContractLength($contractLength)
    {
        $this->contractLength = $contractLength;

        return $this;
    }

    /**
     * Get contractLength
     *
     * @return integer 
     */
    public function getContractLength()
    {
        return $this->contractLength;
    }

    /**
     * Set bedrooms
     *
     * @param integer $bedrooms
     * @return Accommodation
     */
    public function setBedrooms($bedrooms)
    {
        $this->bedrooms = $bedrooms;

        return $this;
    }

    /**
     * Get bedrooms
     *
     * @return integer 
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    /**
     * Set isEnSuite
     *
     * @param boolean $isEnSuite
     * @return Accommodation
     */
    public function setIsEnSuite($isEnSuite)
    {
        $this->isEnSuite = $isEnSuite;

        return $this;
    }

    /**
     * Get isEnSuite
     *
     * @return boolean 
     */
    public function getIsEnSuite()
    {
        return $this->isEnSuite;
    }

    /**
     * Set fullAddress
     *
     * @param string $fullAddress
     * @return Accommodation
     */
    public function setFullAddress($fullAddress)
    {
        $this->fullAddress = $fullAddress;

        return $this;
    }

    /**
     * Get fullAddress
     *
     * @return string 
     */
    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Accommodation
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Accommodation
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Accommodation
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set isVisible
     *
     * @param boolean $isVisible
     * @return Accommodation
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * Get isVisible
     *
     * @return boolean 
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * Set isFeatured
     *
     * @param boolean $isFeatured
     * @return Accommodation
     */
    public function setIsFeatured($isFeatured)
    {
        $this->isFeatured = $isFeatured;

        return $this;
    }

    /**
     * Get isFeatured
     *
     * @return boolean 
     */
    public function getIsFeatured()
    {
        return $this->isFeatured;
    }

    /**
     * Set isPublishable
     *
     * @param boolean $isPublishable
     * @return Accommodation
     */
    public function setIsPublishable($isPublishable)
    {
        $this->isPublishable = $isPublishable;

        return $this;
    }

    /**
     * Get isPublishable
     *
     * @return boolean 
     */
    public function getIsPublishable()
    {
        return $this->isPublishable;
    }

    /**
     * Set publishStartDate
     *
     * @param \DateTime $publishStartDate
     * @return Accommodation
     */
    public function setPublishStartDate($publishStartDate)
    {
        $this->publishStartDate = $publishStartDate;

        return $this;
    }

    /**
     * Get publishStartDate
     *
     * @return \DateTime 
     */
    public function getPublishStartDate()
    {
        return $this->publishStartDate;
    }

    /**
     * Set publishEndDate
     *
     * @param \DateTime $publishEndDate
     * @return Accommodation
     */
    public function setPublishEndDate($publishEndDate)
    {
        $this->publishEndDate = $publishEndDate;

        return $this;
    }

    /**
     * Get publishEndDate
     *
     * @return \DateTime 
     */
    public function getPublishEndDate()
    {
        return $this->publishEndDate;
    }

    /**
     * Add photos
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Photo $photos
     * @return Accommodation
     */
    public function addPhoto(\Westess\RFS\Bundle\AppBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Photo $photos
     */
    public function removePhoto(\Westess\RFS\Bundle\AppBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Add features
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Feature $features
     * @return Accommodation
     */
    public function addFeature(\Westess\RFS\Bundle\AppBundle\Entity\Feature $features)
    {
        $this->features[] = $features;

        return $this;
    }

    /**
     * Remove features
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Feature $features
     */
    public function removeFeature(\Westess\RFS\Bundle\AppBundle\Entity\Feature $features)
    {
        $this->features->removeElement($features);
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Set accommodationType
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\AccommodationType $accommodationType
     * @return Accommodation
     */
    public function setAccommodationType(\Westess\RFS\Bundle\AppBundle\Entity\AccommodationType $accommodationType = null)
    {
        $this->accommodationType = $accommodationType;

        return $this;
    }

    /**
     * Get accommodationType
     *
     * @return \Westess\RFS\Bundle\AppBundle\Entity\AccommodationType 
     */
    public function getAccommodationType()
    {
        return $this->accommodationType;
    }

    /**
     * Set mainPhoto
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Photo $mainPhoto
     * @return Accommodation
     */
    public function setMainPhoto(\Westess\RFS\Bundle\AppBundle\Entity\Photo $mainPhoto = null)
    {
        $this->mainPhoto = $mainPhoto;

        return $this;
    }

    /**
     * Get mainPhoto
     *
     * @return \Westess\RFS\Bundle\AppBundle\Entity\Photo 
     */
    public function getMainPhoto()
    {
        return $this->mainPhoto;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Accommodation
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @var integer
     */
    private $viewCount;


    /**
     * Set viewCount
     *
     * @param integer $viewCount
     * @return Accommodation
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;

        return $this;
    }

    /**
     * Get viewCount
     *
     * @return integer 
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }
}
