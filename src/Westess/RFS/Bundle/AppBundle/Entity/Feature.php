<?php

namespace Westess\RFS\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Feature
 * @ExclusionPolicy("all")
 */
class Feature
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $length;

    /**
     * @var \Westess\RFS\Bundle\AppBundle\Entity\FeatureType
     * @Expose
     * @Type("Westess\RFS\Bundle\AppBundle\Entity\FeatureType")
     */
    private $featureType;

    /**
     * @var \Westess\RFS\Bundle\AppBundle\Entity\Accommodation
     */
    private $accommodation;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set length
     *
     * @param integer $length
     * @return Feature
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return integer
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set featureType
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\FeatureType $featureType
     * @return Feature
     */
    public function setFeatureType(\Westess\RFS\Bundle\AppBundle\Entity\FeatureType $featureType = null)
    {
        $this->featureType = $featureType;

        return $this;
    }

    /**
     * Get featureType
     *
     * @return \Westess\RFS\Bundle\AppBundle\Entity\FeatureType
     */
    public function getFeatureType()
    {
        return $this->featureType;
    }

    /**
     * Set accommodation
     *
     * @param \Westess\RFS\Bundle\AppBundle\Entity\Accommodation $accommodation
     * @return Feature
     */
    public function setAccommodation(\Westess\RFS\Bundle\AppBundle\Entity\Accommodation $accommodation = null)
    {
        $this->accommodation = $accommodation;

        return $this;
    }

    /**
     * Get accommodation
     *
     * @return \Westess\RFS\Bundle\AppBundle\Entity\Accommodation
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }
}
